﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExamenMalesJefferson
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new ExamenMalesJefferson.Paginas.Api.ApiPagina());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
