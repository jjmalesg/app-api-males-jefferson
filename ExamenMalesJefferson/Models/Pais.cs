﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace ExamenMalesJefferson.Models
{
    public class Pais
    {
        [Key]   
        public int id { get; set; }
        public string  nombrePais { get; set; }
        public int poblacionPais { get; set; }
        public string idiomaPais { get; set; }
        public string monedaPais { get; set; }

    }
}
