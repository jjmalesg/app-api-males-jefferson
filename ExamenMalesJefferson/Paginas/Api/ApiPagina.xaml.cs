﻿using ExamenMalesJefferson.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExamenMalesJefferson.Paginas.Api
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApiPagina : ContentPage
    {
        private string Url = "https://apiutnjjmalesg.azurewebsites.net/api/Pais";
        public ApiPagina()
        {
            InitializeComponent();
            lblUltimoId.Text = ObtenerUltimoIdPais().ToString();
        }

        private void Button_Clicked_mostrar(object sender, EventArgs e)
        {
            try
            {
                using (var webclient = new WebClient())
                {
                    lblDatos.Text = "";
                    webclient.Headers.Add("Content-Type", "application/json");
                    var json = webclient.DownloadString(Url + "/" + txtIdPais.Text);
                    var pais = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Pais>(json);
                    if (pais != null)
                    {
                        txtIdPais.Text = pais.id.ToString();
                        txtNombrePais.Text = pais.nombrePais;
                        txtPoblacionPais.Text = pais.poblacionPais.ToString();
                        txtIdiomaPais.Text = pais.idiomaPais;
                        txtMonedaPais.Text = pais.monedaPais;
                    }
                    else
                    {
                        txtNombrePais.Text = "";
                        txtPoblacionPais.Text = "";
                        txtIdiomaPais.Text = "";
                        txtMonedaPais.Text = "";
                    }
                }
            }
            catch (WebException ex)
            {
                lblError.Text = ("No exite el Pais con ese Id o No tienes Conexion");
                txtNombrePais.Text = "";
                txtPoblacionPais.Text = "";
                txtIdiomaPais.Text = "";
                txtMonedaPais.Text = "";
            }
            catch (JsonException ex)
            {
                lblError.Text = ("Ingrese un numero Id");
                txtNombrePais.Text = "";
                txtPoblacionPais.Text = "";
                txtIdiomaPais.Text = "";
                txtMonedaPais.Text = "";
            }
            catch (Exception ex)
            {
                lblError.Text = ("Error general");
                txtNombrePais.Text = "";
                txtPoblacionPais.Text = "";
                txtIdiomaPais.Text = "";
                txtMonedaPais.Text = "";
            }

        }

        private void Button_Clicked_insertar(object sender, EventArgs e)
        {
            try
            {
                using (var webclient = new WebClient())
                {
                    webclient.Headers.Add("Content-Type", "application/json");
                    var datos = new Models.Pais
                    {

                        nombrePais = txtNombrePais.Text,
                        poblacionPais = int.Parse(txtPoblacionPais.Text),
                        idiomaPais = txtIdiomaPais.Text,
                        monedaPais = txtMonedaPais.Text
                    };
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(datos);
                    webclient.UploadString(Url, "POST", json);
                    lblDatos.Text = "INGRESO CORRECTO";
                    lblError.Text = "";
                    lblUltimoId.Text = ObtenerUltimoIdPais().ToString();

                }
            }
            catch (WebException ex)
            {
                lblError.Text=("No hay conexion");
            }
            catch (Exception ex)
            {
                lblError.Text=("Los datos No son correctos o estan incompletos");
            }

        }

        //id = int.Parse(lblUltimoId.Text),

        private void Button_Clicked_actualizar(object sender, EventArgs e)
        {
            try
            {
                using (var webclient = new WebClient())
                {
                    webclient.Headers.Add("Content-Type", "application/json");
                    var datos = new Models.Pais
                    {
                        id = int.Parse(txtIdPais.Text),
                        nombrePais = txtNombrePais.Text,
                        poblacionPais = int.Parse(txtPoblacionPais.Text),
                        idiomaPais = txtIdiomaPais.Text,
                        monedaPais = txtMonedaPais.Text
                    };
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(datos);
                    webclient.UploadString(Url + "/" + txtIdPais.Text, "PUT", json);
                    lblDatos.Text = "ACTUALIZACION CORRECTA";
                    lblError.Text = "";
                }
            }
            catch (WebException ex)
            {
                lblError.Text = ("No hay conexion");
            }
            catch (Exception ex)
            {
                lblError.Text = ("Los datos No son correctos o estan incompletos");
            }

        }
        private void Button_Clicked_borrar(object sender, EventArgs e)
        {
            try
            {
                using (var webclient = new WebClient())
                {
                    webclient.Headers.Add("Content-Type", "application/json");
                    webclient.UploadString(Url + "/" + txtIdPais.Text, "DELETE", "");
                    lblDatos.Text = "ELIMINADO CORRECTAMENTE";

                    txtIdPais.Text = "";
                    txtNombrePais.Text = "";
                    txtPoblacionPais.Text = "";
                    txtIdiomaPais.Text = "";
                    txtMonedaPais.Text = "";
                    lblUltimoId.Text = ObtenerUltimoIdPais().ToString();

                }
            }
            catch (WebException ex)
            {
                lblError.Text=("No exite el Pais con ese Id o No tienes Conexion");
            }
            catch (Exception ex)
            {
                lblError.Text=("Error general: " + ex.Message);
            }

        }

        private Models.Pais[] Paises { get; set; }
       

        private int ObtenerUltimoIdPais()
        {
            using (var webclient = new WebClient())
            {
                webclient.Headers.Add("Content-Type", "application/json");
                var json = webclient.DownloadString(Url); // Obtener todos los países
                Paises = JsonConvert.DeserializeObject<Models.Pais[]>(json);

                if (Paises != null && Paises.Length > 0)
                {
                    return (Paises[Paises.Length-1].id+1);
                }else
                    return 0;
            }
        }

    }
}